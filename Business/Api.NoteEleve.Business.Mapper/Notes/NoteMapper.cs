﻿using Api.NoteEleve.Business.Dto.Eleves;
using Api.NoteEleve.Business.Dto.Notes;
using Api.NoteEleve.Data.Entity.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.NoteEleve.Business.Mapper.Notes
{
    public static class NoteMapper
    {
        /// <summary>
        /// Transforme un le dto CreateEleve en entité eleve.
        /// </summary>
        /// <param name="createEleve">The create eleve.</param>
        /// <returns></returns>
        public static Note TransformCreateDtoToEntity(CreateNoteDto createNote)
        {
            return new Note()
            {
                Valeur = createNote.Valeur,
                IdEleve = createNote.EleveId,
                IdMatiere = createNote.MatiereId,
                DateNote = DateTime.Now,
                DateModification = DateTime.Now,
            };
        }

        /// <summary>
        /// Cette méthode permet de transformer une entité note en Dto de lecture.
        /// </summary>
        /// <param name="note"> la note.</param>
        /// <returns></returns>
        public static ReadNoteDto TransformEntityToReadNoteDto(Note note)
        {
            return new ReadNoteDto()
            {
                Id = note.IdNote,
                EleveId = note.IdEleve,
                EleveCivilite = $"{note.EleveNavigation.Nom} {note.EleveNavigation.Prenom}",
                MatiereId = note.IdMatiere,
                NomMatiere = note.MatiereNavigation.Nom,
                NoteDate = note.DateNote,
                Valeur = note.Valeur,
            };
        }
    }
}
