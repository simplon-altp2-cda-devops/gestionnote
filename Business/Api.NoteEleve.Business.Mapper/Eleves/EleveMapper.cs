﻿using Api.NoteEleve.Business.Dto.Eleves;
using Api.NoteEleve.Data.Entity.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.NoteEleve.Business.Mapper.Eleves
{
    public static class EleveMapper
    {
        /// <summary>
        /// Transforme un le dto CreateEleve en entité eleve.
        /// </summary>
        /// <param name="createEleve">The create eleve.</param>
        /// <returns></returns>
        public static Eleve TransformCreateDtoToEntity(CreateEleveDto createEleve)
        {
            return new Eleve()
            {
                Nom = createEleve.Nom,
                Prenom = createEleve.Prenom,
                DateNaissance = DateOnly.FromDateTime(createEleve.DateDeNaissance),
                IdSexe = createEleve.SexeId,
                IdClasse = createEleve.ClasseId,
            };
        }

        /// <summary>
        /// Cette méthode permet de transformer une entité eleve en Dto de lecture.
        /// </summary>
        /// <param name="eleve">The eleve.</param>
        /// <returns></returns>
        public static ReadEleveDto TransformEntityToReadEleveDto(Eleve eleve)
        {
            return new ReadEleveDto()
            {
                Id = eleve.IdEleve,
                Nom = eleve.Nom,
                Prenom = eleve.Prenom,
                DateDeNaissance = eleve.DateNaissance.ToDateTime(TimeOnly.MinValue),
                SexeId = eleve.IdSexe,
                SexeNom = eleve.SexeNavigation.Genre,
                ClasseId = eleve.IdClasse,
                ClasseNom = eleve.ClasseNavigation.Nom
            };
        }
    }
}
