﻿using Api.NoteEleve.Business.Dto.Eleves;
using Api.NoteEleve.Business.Dto.Notes;
using Api.NoteEleve.Business.Mapper.Eleves;
using Api.NoteEleve.Business.Mapper.Notes;
using Api.NoteEleve.Business.Service.Contract;
using Api.NoteEleve.Data.Repository.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.NoteEleve.Business.Service
{
    public class NoteService : INoteService
    {
        private readonly IEleveRepository _eleveRepository;
        private readonly INoteRepository _noteRepository;
        private readonly ICommonRepository _commonRepository;

        public NoteService(IEleveRepository eleveRepository, INoteRepository noteRepository,
            ICommonRepository commonRepository)
        {
            _eleveRepository = eleveRepository;
            _noteRepository = noteRepository;
            _commonRepository = commonRepository;
        }

        /// <summary>
        /// Cette méthode permet de recupérer la liste des notes des éléves.
        /// </summary>
        /// <returns></returns>
        public async Task<List<ReadNoteDto>> GetNotesAsync()
        {
            var notes = await _noteRepository.GetNotesAsync().ConfigureAwait(false);

            List<ReadNoteDto> readNoteDtos = new List<ReadNoteDto>();

            foreach (var note in notes)
            {
                readNoteDtos.Add(NoteMapper.TransformEntityToReadNoteDto(note));
            }

            return readNoteDtos;
        }

        /// <summary>
        /// Cette méthode permet de créer une note d'un élève dans une matière.
        /// </summary>
        /// <param name="noteDto">les informations de la note.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">
        /// Echec création d'une note : Il n'existe aucun eleve avec cet identifiant = {noteDto.EleveId}
        /// or
        /// Echec création d'une note : Il n'existe aucune matière avec cet identifiant = {noteDto.MatiereId}
        /// </exception>
        public async Task<ReadNoteDto> CreateNoteAsync(CreateNoteDto noteDto)
        {
            var eleve = await _eleveRepository.GetEleveByIdAsync(noteDto.EleveId).ConfigureAwait(false);
            if (eleve == null)
            {
                throw new Exception($"Echec création d'une note : Il n'existe aucun eleve avec cet identifiant = {noteDto.EleveId}");
            }

            var sexe = await _commonRepository.GetMatiereByIdAsync(noteDto.MatiereId).ConfigureAwait(false);
            if (sexe == null)
            {
                throw new Exception($"Echec création d'une note : Il n'existe aucune matière avec cet identifiant = {noteDto.MatiereId}");
            }

            var elementToAdd = NoteMapper.TransformCreateDtoToEntity(noteDto);

            var elementAdded = await _noteRepository.CreateNoteAsync(elementToAdd).ConfigureAwait(false);

            return NoteMapper.TransformEntityToReadNoteDto(elementAdded);
        }


    }
}
