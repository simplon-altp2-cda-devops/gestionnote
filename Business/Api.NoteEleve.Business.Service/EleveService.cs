﻿using Api.NoteEleve.Business.Dto.Eleves;
using Api.NoteEleve.Business.Mapper.Eleves;
using Api.NoteEleve.Business.Service.Contract;
using Api.NoteEleve.Data.Entity.Model;
using Api.NoteEleve.Data.Repository.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.NoteEleve.Business.Service
{
    public class EleveService : IEleveService
    {
        private readonly IEleveRepository _eleveRepository;
        private readonly ICommonRepository _commonRepository;
        private readonly INoteRepository _noteRepository;

        public EleveService(IEleveRepository eleveRepository, ICommonRepository commonRepository, INoteRepository noteRepository)
        {
            _eleveRepository = eleveRepository;
            _commonRepository = commonRepository;
            _noteRepository = noteRepository;
        }

        /// <summary>
        /// Cette méthode permet de recupérer la liste de tous les éléves.
        /// </summary>
        /// <returns></returns>
        public async Task<List<ReadEleveDto>> GetElevesAsync()
        {
            var eleves = await _eleveRepository.GetElevesAsync().ConfigureAwait(false);

            List<ReadEleveDto> readEleveDtos= new List<ReadEleveDto>();

            foreach(var eleve in eleves)
            {
                readEleveDtos.Add(EleveMapper.TransformEntityToReadEleveDto(eleve));
            }

            return readEleveDtos;
        }

        /// <summary>
        /// Cette méthode permet de créer un élève avec ses informations.
        /// </summary>
        /// <param name="eleveDto">les informations de l'élève.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">eleveDto</exception>
        /// <exception cref="System.Exception">
        /// Echec création élève : Il n'existe aucune classe avec cet identifiant = {eleveDto.ClasseId}
        /// or
        /// Echec création élève : Il n'existe aucun sexe avec cet identifiant = {eleveDto.SexeId}
        /// </exception>
        public async Task<ReadEleveDto> CreateEleveAsync(CreateEleveDto eleveDto)
        {
            if(eleveDto == null)
            {
                throw new ArgumentNullException(nameof(eleveDto));
            }

            var classe = await _commonRepository.GetClasseByIdAsync(eleveDto.ClasseId).ConfigureAwait(false);
            if(classe == null)
            {
                throw new Exception($"Echec création d'un élève : Il n'existe aucune classe avec cet identifiant = {eleveDto.ClasseId}");
            }

            var sexe = await _commonRepository.GetSexeByIdAsync(eleveDto.SexeId).ConfigureAwait(false);
            if (sexe == null)
            {
                throw new Exception($"Echec création élève : Il n'existe aucun sexe avec cet identifiant = {eleveDto.SexeId}");
            }

            var eleveToAdd = EleveMapper.TransformCreateDtoToEntity(eleveDto);

            var eleveAdded = await _eleveRepository.CreateEleveAsync(eleveToAdd).ConfigureAwait(false);

            return EleveMapper.TransformEntityToReadEleveDto(eleveAdded);
        }

        /// <summary>
        /// Cette méthode permet de supprimer les informations d'un élève.
        /// </summary>
        /// <param name="eleveId">The eleve identifier.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">
        /// Echec de suppression d'un élève : Il n'existe aucun élève avec cet identifiant = {eleveId}
        /// or
        /// Echec de suppression d'un élève : L'élève avec l'identifiant = {eleveId} possède des notes
        /// </exception>
        public async Task<ReadEleveDto> DeleteEleveAsync(int eleveId)
        {
            var eleve = await _eleveRepository.GetEleveByIdAsync(eleveId).ConfigureAwait(false);
            if (eleve == null)
            {
                throw new Exception($"Echec de suppression d'un élève : Il n'existe aucun élève avec cet identifiant = {eleveId}");
            }

            var eleveNotes = await _noteRepository.GetNotesAsync(eleveId).ConfigureAwait(false);
            if(eleveNotes?.Count >= 1)
            {
                throw new Exception($"Echec de suppression d'un élève : L'élève avec l'identifiant = {eleveId} possède des notes");
            }

            var eleveDeleted = await _eleveRepository.DeleteEleveAsync(eleve).ConfigureAwait(false);

            return EleveMapper.TransformEntityToReadEleveDto(eleveDeleted);
        }


    }
}
