﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.NoteEleve.Business.Dto.Eleves
{
    public class ReadEleveDto : CreateEleveDto
    {
        /// <summary>
        /// Identifiant de l'élève.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int Id { get; set; }

        /// <summary>
        ///le nom de la classe de l'élève.
        /// </summary>
        /// <value>
        /// The classe nom.
        /// </value>
        public string ClasseNom { get; set; }

        /// <summary>
        /// Le nom du sexe de l'élève.
        /// </summary>
        /// <value>
        /// The sexe nom.
        /// </value>
        public string SexeNom { get; set; }


    }
}
