﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.NoteEleve.Business.Dto.Eleves
{
    public class CreateEleveDto
    {
        /// <summary>
        /// Le nom de l'élève
        /// </summary>
        /// <value>
        /// The nom.
        /// </value>
        public string Nom { get; set; }

        /// <summary>
        /// Le prenom de l'élève.
        /// </summary>
        /// <value>
        /// The prenom.
        /// </value>
        public string Prenom { get; set; }

        /// <summary>
        /// La date de naissance de l'élève.
        /// </summary>
        /// <value>
        /// The date de naissance.
        /// </value>
        public DateTime DateDeNaissance { get; set; }

        /// <summary>
        /// l'identifiant de la classe de l'élève.
        /// </summary>
        /// <value>
        /// The classe identifier.
        /// </value>
        public int ClasseId { get; set; }

        /// <summary>
        /// l'identifiant du sexe de l'élève.
        /// </summary>
        /// <value>
        /// The sexe identifier.
        /// </value>
        public int SexeId { get; set; }
    }
}
