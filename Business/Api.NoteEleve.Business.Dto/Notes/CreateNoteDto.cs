﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.NoteEleve.Business.Dto.Notes
{
    public class CreateNoteDto
    {
        public double Valeur { get; set; }

        public int EleveId { get; set; }

        public int MatiereId { get; set; }
    }
}
