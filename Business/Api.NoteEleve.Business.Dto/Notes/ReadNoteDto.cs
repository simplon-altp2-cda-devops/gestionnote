﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.NoteEleve.Business.Dto.Notes
{
    public class ReadNoteDto : CreateNoteDto
    {
        /// <summary>
        /// l'identifiant de la note.
        /// </summary>
        /// <value>
        /// The identifier.
        /// </value>
        public int Id { get; set; }

        /// <summary>
        ///le nom et prénom de l'élève.
        /// </summary>
        public string EleveCivilite { get; set; }

        /// <summary>
        /// le nom de la matiere.
        /// </summary>
        /// <value>
        /// The nom matiere.
        /// </value>
        public string NomMatiere { get; set; }

        /// <summary>
        /// la date de la note.
        /// </summary>
        /// <value>
        /// The note date.
        /// </value>
        public DateTime NoteDate { get; set; }

    }
}
