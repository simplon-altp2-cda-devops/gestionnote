﻿using Api.NoteEleve.Business.Dto.Notes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.NoteEleve.Business.Service.Contract
{
    public interface INoteService
    {
        /// <summary>
        /// Cette méthode permet de créer une note d'un élève dans une matière.
        /// </summary>
        /// <param name="noteDto">les informations de la note.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">
        /// Echec création d'une note : Il n'existe aucun eleve avec cet identifiant = {noteDto.EleveId}
        /// or
        /// Echec création d'une note : Il n'existe aucune matière avec cet identifiant = {noteDto.MatiereId}
        /// </exception>
        Task<ReadNoteDto> CreateNoteAsync(CreateNoteDto noteDto);

        /// <summary>
        /// Cette méthode permet de recupérer la liste des notes des éléves.
        /// </summary>
        /// <returns></returns>
        Task<List<ReadNoteDto>> GetNotesAsync();
    }
}
