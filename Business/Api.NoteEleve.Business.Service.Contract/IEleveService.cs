﻿using Api.NoteEleve.Business.Dto.Eleves;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.NoteEleve.Business.Service.Contract
{
    public interface IEleveService
    {
        /// <summary>
        /// Cette méthode permet de créer un élève avec ses informations.
        /// </summary>
        /// <param name="eleveDto">les informations de l'élève.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">eleveDto</exception>
        /// <exception cref="System.Exception">
        /// Echec création élève : Il n'existe aucune classe avec cet identifiant = {eleveDto.ClasseId}
        /// or
        /// Echec création élève : Il n'existe aucun sexe avec cet identifiant = {eleveDto.SexeId}
        /// </exception>
        Task<ReadEleveDto> CreateEleveAsync(CreateEleveDto eleveDto);

        /// <summary>
        /// Cette méthode permet de recupérer la liste de tous les éléves.
        /// </summary>
        /// <returns></returns>
        Task<List<ReadEleveDto>> GetElevesAsync();

        /// <summary>
        /// Cette méthode permet de supprimer les informations d'un élève.
        /// </summary>
        /// <param name="eleveId">The eleve identifier.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception">
        /// Echec de suppression d'un élève : Il n'existe aucun élève avec cet identifiant = {eleveId}
        /// or
        /// Echec de suppression d'un élève : L'élève avec l'identifiant = {eleveId} possède des notes
        /// </exception>
        Task<ReadEleveDto> DeleteEleveAsync(int eleveId);
    }
}
