﻿using Api.NoteEleve.Business.Dto.Eleves;
using Api.NoteEleve.Business.Dto.Notes;
using Api.NoteEleve.Business.Service.Contract;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Api.NoteEleve.Application.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NotesController : ControllerBase
    {
        private readonly INoteService _noteService;

        public NotesController(INoteService noteService)
        {
            _noteService = noteService;
        }


        /// <summary>
        /// Ressource pour recupérer la liste des notes
        /// </summary>
        /// <returns></returns>
        // GET: api/<NotesController>
        [HttpGet]
        [ProducesResponseType(typeof(List<ReadNoteDto>), 200)]
        public async Task<ActionResult> GetNotesAsync()
        {
            var notesDto = await _noteService.GetNotesAsync().ConfigureAwait(false);
            return Ok(notesDto);
        }


        /// <summary>
        /// Ressource pour ajouter une nouvelle note d'un élève
        /// </summary>
        /// <param name="eleveDto">les informations de la note de l'élève</param>
        /// <returns></returns>
        // POST api/<NotesController>
        [HttpPost]
        [ProducesResponseType(typeof(ReadNoteDto), 200)]
        public async Task<ActionResult> CreateNoteAsync([FromBody] CreateNoteDto noteDto)
        {
            try
            {
                var elementAdded = await _noteService.CreateNoteAsync(noteDto).ConfigureAwait(false);
                return Ok(elementAdded);
            }
            catch (Exception e)
            {
                return BadRequest(new
                {
                    Error = e.Message
                });
            }


        }

        // POST api/<NotesController>
        /* [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<NotesController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<NotesController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }*/
    }
}
