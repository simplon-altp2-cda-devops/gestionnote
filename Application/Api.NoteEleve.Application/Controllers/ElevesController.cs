﻿using Api.NoteEleve.Business.Dto.Eleves;
using Api.NoteEleve.Business.Service.Contract;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Api.NoteEleve.Application.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ElevesController : ControllerBase
    {

        private readonly IEleveService _eleveService;

        public ElevesController(IEleveService eleveService)
        {
            _eleveService = eleveService;
        }


        /// <summary>
        /// Ressource pour recupérer la liste des éléves
        /// </summary>
        /// <returns></returns>
        // GET: api/<ElevesController>
        [HttpGet]
        [ProducesResponseType(typeof(List<ReadEleveDto>), 200)]
        public async Task<ActionResult> GetElevesAsync()
        {
            var elevesDto = await _eleveService.GetElevesAsync().ConfigureAwait(false);
            return Ok(elevesDto);
        }


        /// <summary>
        /// Ressource pour ajouter un nouveau élève
        /// </summary>
        /// <param name="eleveDto">les informations du nouveau élève</param>
        /// <returns></returns>
        // POST api/<ElevesController>
        [HttpPost]
        public async Task<ActionResult> CreateEleveAsync([FromBody] CreateEleveDto eleveDto)
        {

            if(string.IsNullOrWhiteSpace(eleveDto?.Nom) || string.IsNullOrWhiteSpace(eleveDto?.Prenom))
            {
                return BadRequest(new {
                    Error = "Les informations utiles pour la création d'un élève sont vides"
                });
            }


            try
            {
                var elementAdded = await _eleveService.CreateEleveAsync(eleveDto).ConfigureAwait(false);
                return Ok(elementAdded);
            }
            catch(Exception e)
            {
                return BadRequest(new
                {
                    Error = e.Message
                });
            }


        }

        /// <summary>
        /// Ressource pour supprimer un élève
        /// </summary>
        /// <param name="id">l'identifiant  de l'élève</param>
        /// <returns></returns>
        // DELETE api/<ElevesController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteEleveAsync(int id)
        {
            try
            {
                var elementDeleted = await _eleveService.DeleteEleveAsync(id).ConfigureAwait(false);
                return Ok(elementDeleted);
            }
            catch (Exception e)
            {
                return BadRequest(new
                {
                    Error = e.Message
                });
            }
        }

        // GET api/<ElevesController>/5
        /* [HttpGet("{id}")]
         public string Get(int id)
         {
             return "value";
         }
         // PUT api/<ElevesController>/5
         [HttpPut("{id}")]
         public void Put(int id, [FromBody] string value)
         {
         }

         */
    }
}
