using Api.NoteEleve.Data.Context.Contract;
using Api.NoteEleve.Data.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Api.NoteEleve.Data.Repository;
using Api.NoteEleve.Data.Repository.Contract;
using Api.NoteEleve.Business.Service.Contract;
using Api.NoteEleve.Business.Service;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);
var configuration = builder.Configuration;

// Connexion  � la base de donn�es
string connectionString = configuration.GetConnectionString("BddConnection");
builder.Services.AddDbContext<INoteEleveDBContext, NoteEleveDBContext>(
    options => options.UseMySql(connectionString, ServerVersion.AutoDetect(connectionString))
);

// IOC des repositories

builder.Services.AddScoped<INoteRepository, NoteRepository>();
builder.Services.AddScoped<IEleveRepository, EleveRepository>();
builder.Services.AddScoped<ICommonRepository, CommonRepository>();


// IOC des services

builder.Services.AddScoped<IEleveService, EleveService>();
builder.Services.AddScoped<INoteService, NoteService>();


// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

// Inculde xml comment on the swagger view
var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";

builder.Services.AddSwaggerGen(options =>
    options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename))
);


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
