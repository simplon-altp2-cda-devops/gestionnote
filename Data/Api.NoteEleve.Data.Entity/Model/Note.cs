﻿using System;
using System.Collections.Generic;

namespace Api.NoteEleve.Data.Entity.Model;

public partial class Note
{
    public int IdNote { get; set; }

    public double Valeur { get; set; }

    public DateTime DateNote { get; set; }

    public DateTime DateModification { get; set; }

    public int IdMatiere { get; set; }

    public int IdEleve { get; set; }

    public virtual Eleve EleveNavigation { get; set; } = null!;

    public virtual Matiere MatiereNavigation { get; set; } = null!;
}
