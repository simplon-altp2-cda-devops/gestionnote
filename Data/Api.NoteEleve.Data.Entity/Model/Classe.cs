﻿using System;
using System.Collections.Generic;

namespace Api.NoteEleve.Data.Entity.Model;

public partial class Classe
{
    public int IdClasse { get; set; }

    public string Nom { get; set; } = null!;

    public virtual ICollection<Eleve> Eleves { get; set; } = new List<Eleve>();
}
