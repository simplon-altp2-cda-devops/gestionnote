﻿using System;
using System.Collections.Generic;

namespace Api.NoteEleve.Data.Entity.Model;

public partial class Matiere
{
    public int IdMatiere { get; set; }

    public string Nom { get; set; } = null!;

    public virtual ICollection<Note> Notes { get; set; } = new List<Note>();
}
