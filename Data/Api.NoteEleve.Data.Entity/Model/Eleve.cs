﻿using System;
using System.Collections.Generic;

namespace Api.NoteEleve.Data.Entity.Model;

public partial class Eleve
{
    public int IdEleve { get; set; }

    public string Nom { get; set; } = null!;

    public string Prenom { get; set; } = null!;

    public DateOnly DateNaissance { get; set; }

    public int IdClasse { get; set; }

    public int IdSexe { get; set; }

    public virtual Classe ClasseNavigation { get; set; } = null!;

    public virtual Sexe SexeNavigation { get; set; } = null!;

    public virtual ICollection<Note> Notes { get; set; } = new List<Note>();
}
