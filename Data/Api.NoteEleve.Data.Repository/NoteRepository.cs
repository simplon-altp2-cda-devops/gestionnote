﻿using Api.NoteEleve.Data.Context.Contract;
using Api.NoteEleve.Data.Entity.Model;
using Api.NoteEleve.Data.Repository.Contract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.NoteEleve.Data.Repository
{
    public class NoteRepository : INoteRepository
    {
        private readonly INoteEleveDBContext _dBContext;

        public NoteRepository(INoteEleveDBContext dbContext)
        {
            _dBContext = dbContext;
        }

        /// <summary>
        /// Cette méthode permet de créer une note d'un élève dans une matière.
        /// </summary>
        /// <param name="noteToAdd">la note à ajouter.</param>
        /// <returns></returns>
        public async Task<Note> CreateNoteAsync(Note noteToAdd)
        {
            var elementAdded = await _dBContext.Notes.AddAsync(noteToAdd).ConfigureAwait(false);
            await _dBContext.SaveChangesAsync().ConfigureAwait(false);

            return elementAdded.Entity;
        }

        /// <summary>
        /// Cette méthode permet de supprimer une note d'un élève.
        /// </summary>
        /// <param name="noteToDelete">la note à supprimer.</param>
        /// <returns></returns>
        public async Task<Note> DeleteNoteAsync(Note noteToDelete)
        {
            var elementDeleted = _dBContext.Notes.Remove(noteToDelete);
            await _dBContext.SaveChangesAsync().ConfigureAwait(false);

            return elementDeleted.Entity;
        }

        /// <summary>
        /// Cette méthode permet de modifier une note.
        /// </summary>
        /// <param name="noteToUpdate">la note à modifier.</param>
        /// <returns></returns>
        public async Task<Note> UpdateNoteAsync(Note noteToUpdate)
        {
            var elementUptated = _dBContext.Notes.Update(noteToUpdate);
            await _dBContext.SaveChangesAsync().ConfigureAwait(false);

            return elementUptated.Entity;
        }

        /// <summary>
        /// Cette méthode permet de recupérer la liste des notes des élèves.
        /// </summary>
        /// <returns></returns>
        public async Task<List<Note>> GetNotesAsync()
        {
            return await _dBContext.Notes
                  .Include(x => x.EleveNavigation)
                  .Include(x => x.MatiereNavigation)
                  .ToListAsync()
                  .ConfigureAwait(false);
        }

        /// <summary>
        /// Cette méthode permet de recupérer la liste des notes d'un élève.
        /// </summary>
        /// <returns></returns>
        public async Task<List<Note>> GetNotesAsync(int eleveId)
        {
            return await _dBContext.Notes
                  .Include(x => x.EleveNavigation)
                  .Include(x => x.MatiereNavigation)
                  .Where(x => x.IdEleve == eleveId)
                  .ToListAsync()
                  .ConfigureAwait(false);
        }

        /// <summary>
        /// Cette méthode permet de recupérer les information d'une note d'un élève par son identifiant.
        /// </summary>
        /// <param name="eleveId">Identifiant de la note.</param>
        /// <returns></returns>
        public async Task<Note> GetNoteByIdAsync(int noteId)
        {
            return await _dBContext.Notes
                  .Include(x => x.EleveNavigation)
                  .Include(x => x.MatiereNavigation)
               .FirstOrDefaultAsync(x => x.IdNote == noteId)
               .ConfigureAwait(false);
        }
    }
}
