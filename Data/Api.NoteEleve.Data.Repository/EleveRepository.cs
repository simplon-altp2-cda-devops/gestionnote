﻿using Api.NoteEleve.Data.Context.Contract;
using Api.NoteEleve.Data.Entity.Model;
using Api.NoteEleve.Data.Repository.Contract;
using Microsoft.EntityFrameworkCore;

namespace Api.NoteEleve.Data.Repository
{
    public class EleveRepository : IEleveRepository
    {
        private readonly INoteEleveDBContext _dBContext;

        public EleveRepository (INoteEleveDBContext dbContext)
        {
            _dBContext = dbContext;
        }

        /// <summary>
        /// Cette méthode permet de créer un élève.
        /// </summary>
        /// <param name="eleveToAdd">le nouvel elève à ajouter.</param>
        /// <returns></returns>
        public async Task<Eleve> CreateEleveAsync(Eleve eleveToAdd)
        {
            var elementAdded = await _dBContext.Eleves.AddAsync(eleveToAdd).ConfigureAwait(false);
            await _dBContext.SaveChangesAsync().ConfigureAwait(false);

            return elementAdded.Entity;
        }

        /// <summary>
        /// Cette méthode permet de supprimer un élève.
        /// </summary>
        /// <param name="eleveToDelete">l'élève à supprimer.</param>
        /// <returns></returns>
        public async Task<Eleve> DeleteEleveAsync(Eleve eleveToDelete)
        {
            var elementDeleted =  _dBContext.Eleves.Remove(eleveToDelete);
            await _dBContext.SaveChangesAsync().ConfigureAwait(false);

            return elementDeleted.Entity;
        }

        /// <summary>
        /// Cette méthode permet de moidifer un élève.
        /// </summary>
        /// <param name="eleveToUpdate">l'élève à modifier.</param>
        /// <returns></returns>
        public async Task<Eleve> UpdateEleveAsync(Eleve eleveToUpdate)
        {
            var elementUptated = _dBContext.Eleves.Update(eleveToUpdate);
            await _dBContext.SaveChangesAsync().ConfigureAwait(false);

            return elementUptated.Entity;
        }

        /// <summary>
        /// Cette méthode permet de recupérer la liste des élèves.
        /// </summary>
        /// <returns></returns>
        public async Task<List<Eleve>> GetElevesAsync()
        {
          return await _dBContext.Eleves
                .Include(x => x.ClasseNavigation)
                .Include( x=> x.SexeNavigation)
                .ToListAsync()
                .ConfigureAwait(false);
        }

        /// <summary>
        /// Cette méthode permet de recupérer les information d'un élève par son identifiant.
        /// </summary>
        /// <param name="eleveId">Identifiant de l'élève.</param>
        /// <returns></returns>
        public async Task<Eleve> GetEleveByIdAsync(int eleveId)
        {
            return await _dBContext.Eleves
               .Include(x => x.ClasseNavigation)
               .Include(x => x.SexeNavigation)
               .Include(x => x.Notes)
               .FirstOrDefaultAsync(x => x.IdEleve == eleveId)
               .ConfigureAwait(false);
        }
    }
}
