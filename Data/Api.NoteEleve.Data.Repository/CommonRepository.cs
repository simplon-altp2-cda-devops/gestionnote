﻿using Api.NoteEleve.Data.Context.Contract;
using Api.NoteEleve.Data.Entity.Model;
using Api.NoteEleve.Data.Repository.Contract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.NoteEleve.Data.Repository
{
    public class CommonRepository : ICommonRepository
    {
        private readonly INoteEleveDBContext _dBContext;

        public CommonRepository(INoteEleveDBContext dbContext)
        {
            _dBContext = dbContext;
        }

        /// <summary>
        /// Cette méthode permet de recupérer les information du sexe par son identifiant.
        /// </summary>
        /// <param name="sexeId">Identifiant du sexe.</param>
        /// <returns></returns>
        public async Task<Sexe> GetSexeByIdAsync(int sexeId)
        {
            return await _dBContext.Sexes
               .FirstOrDefaultAsync(x => x.IdSexe == sexeId)
               .ConfigureAwait(false);
        }

        /// <summary>
        /// Cette méthode permet de recupérer les information du classe par son identifiant.
        /// </summary>
        /// <param name="classeId">Identifiant de la classe.</param>
        /// <returns></returns>
        public async Task<Classe> GetClasseByIdAsync(int classeId)
        {
            return await _dBContext.Classes
               .FirstOrDefaultAsync(x => x.IdClasse == classeId)
               .ConfigureAwait(false);
        }

        /// <summary>
        /// Cette méthode permet de recupérer les information d'une matiere par son identifiant.
        /// </summary>
        /// <param name="matiereId">Identifiant de la matiere.</param>
        /// <returns></returns>
        public async Task<Matiere> GetMatiereByIdAsync(int matiereId)
        {
            return await _dBContext.Matieres
               .FirstOrDefaultAsync(x => x.IdMatiere == matiereId)
               .ConfigureAwait(false);
        }
    }
}
