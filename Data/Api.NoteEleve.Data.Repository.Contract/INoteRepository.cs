﻿using Api.NoteEleve.Data.Entity.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.NoteEleve.Data.Repository.Contract
{
    public interface INoteRepository
    {
        /// <summary>
        /// Cette méthode permet de créer une note d'un élève dans une matière.
        /// </summary>
        /// <param name="noteToAdd">la note à ajouter.</param>
        /// <returns></returns>
        Task<Note> CreateNoteAsync(Note noteToAdd);

        /// <summary>
        /// Cette méthode permet de supprimer une note d'un élève.
        /// </summary>
        /// <param name="noteToDelete">la note à supprimer.</param>
        /// <returns></returns>
        Task<Note> DeleteNoteAsync(Note noteToDelete);

        /// <summary>
        /// Cette méthode permet de modifier une note.
        /// </summary>
        /// <param name="noteToUpdate">la note à modifier.</param>
        /// <returns></returns>
        Task<Note> UpdateNoteAsync(Note noteToUpdate);

        /// <summary>
        /// Cette méthode permet de recupérer la liste des notes des élèves.
        /// </summary>
        /// <returns></returns>
        Task<List<Note>> GetNotesAsync();

        /// <summary>
        /// Cette méthode permet de recupérer la liste des notes d'un élève.
        /// </summary>
        /// <returns></returns>
        Task<List<Note>> GetNotesAsync(int eleveId);

        /// <summary>
        /// Cette méthode permet de recupérer les information d'une note d'un élève par son identifiant.
        /// </summary>
        /// <param name="eleveId">Identifiant de la note.</param>
        /// <returns></returns>
        Task<Note> GetNoteByIdAsync(int noteId);
    }
}
