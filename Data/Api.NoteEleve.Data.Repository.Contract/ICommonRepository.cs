﻿using Api.NoteEleve.Data.Entity.Model;

namespace Api.NoteEleve.Data.Repository.Contract
{
    public interface ICommonRepository
    {
        /// <summary>
        /// Cette méthode permet de recupérer les information du sexe par son identifiant.
        /// </summary>
        /// <param name="sexeId">Identifiant du sexe.</param>
        /// <returns></returns>
        Task<Sexe> GetSexeByIdAsync(int sexeId);

        /// <summary>
        /// Cette méthode permet de recupérer les information du classe par son identifiant.
        /// </summary>
        /// <param name="classeId">Identifiant de la classe.</param>
        /// <returns></returns>
        Task<Classe> GetClasseByIdAsync(int classeId);

        /// <summary>
        /// Cette méthode permet de recupérer les information d'une matiere par son identifiant.
        /// </summary>
        /// <param name="matiereId">Identifiant de la matiere.</param>
        /// <returns></returns>
        Task<Matiere> GetMatiereByIdAsync(int matiereId);
    }
}
