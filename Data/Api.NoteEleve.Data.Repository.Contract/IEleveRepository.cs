﻿using Api.NoteEleve.Data.Entity.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.NoteEleve.Data.Repository.Contract
{
    public interface IEleveRepository
    {
        /// <summary>
        /// Cette méthode permet de créer un élève.
        /// </summary>
        /// <param name="eleveToAdd">le nouvel elève à ajouter.</param>
        /// <returns></returns>
        Task<Eleve> CreateEleveAsync(Eleve eleveToAdd);

        /// <summary>
        /// Cette méthode permet de supprimer un élève.
        /// </summary>
        /// <param name="eleveToDelete">l'élève à supprimer.</param>
        /// <returns></returns>
        Task<Eleve> DeleteEleveAsync(Eleve eleveToDelete);

        /// <summary>
        /// Cette méthode permet de moidifer un élève.
        /// </summary>
        /// <param name="eleveToUpdate">l'élève à modifier.</param>
        /// <returns></returns>
        Task<Eleve> UpdateEleveAsync(Eleve eleveToUpdate);

        /// <summary>
        /// Cette méthode permet de recupérer la liste des élèves.
        /// </summary>
        /// <returns></returns>
        Task<List<Eleve>> GetElevesAsync();

        /// <summary>
        /// Cette méthode permet de recupérer les information d'un élève par son identifiant.
        /// </summary>
        /// <param name="eleveId">Identifiant de l'élève.</param>
        /// <returns></returns>
        Task<Eleve> GetEleveByIdAsync(int eleveId);
    }
}
