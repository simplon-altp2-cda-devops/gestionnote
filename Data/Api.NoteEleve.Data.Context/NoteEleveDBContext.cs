﻿using System;
using System.Collections.Generic;
using Api.NoteEleve.Data.Context.Contract;
using Api.NoteEleve.Data.Entity.Model;
using Microsoft.EntityFrameworkCore;

namespace Api.NoteEleve.Data.Entity;

public partial class NoteEleveDBContext : DbContext , INoteEleveDBContext
{
    public NoteEleveDBContext()
    {
    }

    public NoteEleveDBContext(DbContextOptions<NoteEleveDBContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Classe> Classes { get; set; }

    public virtual DbSet<Eleve> Eleves { get; set; }

    public virtual DbSet<Matiere> Matieres { get; set; }

    public virtual DbSet<Note> Notes { get; set; }

    public virtual DbSet<Sexe> Sexes { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder
            .UseCollation("utf8mb4_0900_ai_ci")
            .HasCharSet("utf8mb4");

        modelBuilder.Entity<Classe>(entity =>
        {
            entity.HasKey(e => e.IdClasse).HasName("PRIMARY");

            entity.ToTable("classe");

            entity.HasIndex(e => e.Nom, "Nom").IsUnique();

            entity.Property(e => e.Nom).HasMaxLength(50);
        });

        modelBuilder.Entity<Eleve>(entity =>
        {
            entity.HasKey(e => e.IdEleve).HasName("PRIMARY");

            entity.ToTable("eleve");

            entity.HasIndex(e => e.IdClasse, "IdClasse");

            entity.HasIndex(e => e.IdSexe, "IdSexe");

            entity.Property(e => e.Nom).HasMaxLength(50);
            entity.Property(e => e.Prenom).HasMaxLength(100);

            entity.HasOne(d => d.ClasseNavigation).WithMany(p => p.Eleves)
                .HasForeignKey(d => d.IdClasse)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("eleve_ibfk_1");

            entity.HasOne(d => d.SexeNavigation).WithMany(p => p.Eleves)
                .HasForeignKey(d => d.IdSexe)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("eleve_ibfk_2");
        });

        modelBuilder.Entity<Matiere>(entity =>
        {
            entity.HasKey(e => e.IdMatiere).HasName("PRIMARY");

            entity.ToTable("matiere");

            entity.HasIndex(e => e.Nom, "Nom").IsUnique();

            entity.Property(e => e.Nom).HasMaxLength(50);
        });

        modelBuilder.Entity<Note>(entity =>
        {
            entity.HasKey(e => e.IdNote).HasName("PRIMARY");

            entity.ToTable("note");

            entity.HasIndex(e => e.IdEleve, "IdEleve");

            entity.HasIndex(e => e.IdMatiere, "IdMatiere");

            entity.Property(e => e.DateModification).HasColumnType("datetime");
            entity.Property(e => e.DateNote).HasColumnType("datetime");

            entity.HasOne(d => d.EleveNavigation).WithMany(p => p.Notes)
                .HasForeignKey(d => d.IdEleve)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("note_ibfk_2");

            entity.HasOne(d => d.MatiereNavigation).WithMany(p => p.Notes)
                .HasForeignKey(d => d.IdMatiere)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("note_ibfk_1");
        });

        modelBuilder.Entity<Sexe>(entity =>
        {
            entity.HasKey(e => e.IdSexe).HasName("PRIMARY");

            entity.ToTable("sexe");

            entity.HasIndex(e => e.Genre, "Genre").IsUnique();

            entity.Property(e => e.Genre).HasMaxLength(50);
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
