﻿using Api.NoteEleve.Data.Entity.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.NoteEleve.Data.Context.Contract
{
    public interface INoteEleveDBContext : IDbContext
    {
        DbSet<Classe> Classes { get; set; }

        DbSet<Eleve> Eleves { get; set; }

        DbSet<Matiere> Matieres { get; set; }

        DbSet<Note> Notes { get; set; }

        DbSet<Sexe> Sexes { get; set; }
    }
}
